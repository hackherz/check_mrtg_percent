#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <time.h>

#include <clara.hpp>

using namespace std;
using namespace clara;


// Return values
const int STATUS_OK       = 0;
const int STATUS_WARNING  = 1;
const int STATUS_UNKNOWN  = 3;
const int STATUS_CRITICAL = 2;





// Put the traffic in a format which is nice to read for humans
const string humanizeTraffic(unsigned int traffic) {
	if(traffic < 1024) {
		return to_string(traffic) + "Bs";
	}

	unsigned int copy = traffic;
	unsigned int exponent = 0;
	float teiler = 1;

	while(copy > 1024) {
		copy /= 1024;
		exponent += 3;
		teiler *= 1024;
	}

	char prefix;

	switch(exponent) {
	case 3:
		prefix = 'K';
		break;
	case 6:
		prefix = 'M';
		break;
	case 9:
		prefix = 'G';
		break;
	case 12:
		prefix = 'T';
		break;
	deafult:
		prefix = '?';
	}

	char buf[100];
	sprintf(buf, "%4.2f %cB/s", (float) traffic / teiler, prefix);

	return buf;

}

string getPercentage(unsigned int zaehler, unsigned int nenner) {
	float percentage = (float) zaehler / (float) nenner * 100.0;
	char buf[100];
	sprintf(buf, "%4.2f", percentage);

	string ret(buf);

	return ret + '%';
}



int main(int argc, char **argv) {
	bool help = false;
	bool showVersion = false;
	unsigned int maximum = 125000000; // Default to Gigabit
	unsigned int expires = 0;
	string filename;
	string aggregation = "AVG";
	string warningString = "90,90";
	string criticalString = "95,95";

	// Command line arguments
	auto cli = Help(help)
		| Opt(showVersion)["-V"]["--version"]("Print version information")
		| Opt(maximum, "INTEGER,INTEGER")["-m"]["--max"]("Maximum speed pair in Byte/s <incoming,outgoing>")
		| Opt(filename, "STRING")["-F"]["--filename"]("File to read log from")
		| Opt(expires, "INTEGER")["-e"]["--expires"]("Minutes after which log expires")
		| Opt(aggregation, "AVG|MAX")["-a"]["--aggregation"]("Test average or maximum")
		| Opt(warningString, "INTEGER,INTEGER")["-w"]["--warning"]("Warning threshold percentage pair <incoming>,<outgoing>")
		| Opt(criticalString, "INTEGER,INTEGER")["-c"]["--critical"]("Critical threshold percentage pair <incoming>,<outgoing>");

	auto result = cli.parse(Args(argc, argv));

	if(!result) {
		cerr << "Error: " << result.errorMessage() << endl;
		return STATUS_UNKNOWN;
	}


	// Display help
	if(help) {
		cli.writeToStream(cout);
		return 0;
	}


	// Open the file
	ifstream dat_ein;
	dat_ein.open(filename.c_str(), ios_base::in);

	if(!dat_ein.is_open()) {
		cerr << "Could not open file." << endl;
		return STATUS_UNKNOWN;
	}

	// Read first and second line
	string zeilen[2];
	int counter = 0;
	while(counter < 2 && !dat_ein.eof()) {
		getline(dat_ein, zeilen[counter++]);
	}

	dat_ein.close();

	if(counter != 2) {
		cerr << "Fileformat is wrong." << endl;
		return STATUS_UNKNOWN;
	}


	// Parse line
	stringstream input_stringstream(zeilen[1]);
	string word;
	counter = 0;
	unsigned int fields[5];
	while(getline(input_stringstream, word, ' ') && counter < sizeof(fields)) {
		fields[counter++] = stoi(word);
	}


	// Need to parse timestamp
	if(expires > 0) {
		stringstream timestream(zeilen[0]);
		getline(timestream, word, ' ');

		time_t timer;
		time(&timer); // current timestamp

		if((timer - stoi(word))/60 >= expires) {
			cerr << "Timestamp is too old." << endl;
			return STATUS_CRITICAL;
		}
	}


	// Save the values
	unsigned int incoming = 0, outgoing = 0;
	if(aggregation == "AVG") {
		incoming = fields[1];
		outgoing = fields[2];
	} else {
		incoming = fields[3];
		outgoing = fields[4];
	}



	// Output
	// inUsage=21103.42%;85;98 outUsage=226363.55%;85;98 inBandwidth=105.52MBs outBandwidth=212.22MBs
	cout << "Traffic OK - In = " << humanizeTraffic(incoming) << " , Out = " << humanizeTraffic(outgoing);
	cout << " | inUsage=" << getPercentage(incoming, maximum) << ";85;98 ";
	cout << "outUsage=" << getPercentage(outgoing, maximum) << ";85;98 ";
	cout << "inBandwidth=" << humanizeTraffic(incoming);
	cout << " outBandwidth=" << humanizeTraffic(outgoing) << endl;



	return STATUS_OK;
}
